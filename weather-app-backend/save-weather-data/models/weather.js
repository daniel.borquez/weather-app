const db = require('../db');

function WeatherModel(weatherKey, temperature, summary, icon) {

    this.weatherKey = weatherKey,
    this.weatherObject = {
        temperature: temperature,
        summary: summary,
        icon: icon
    }


    this.find = function () {
        return new Promise((resolve, reject) => {
            
            db.client.hgetall(this.weatherKey, (error, weather) => {

                if (!error) {
                    resolve(weather);

                } else {
                    reject('Error al buscar WeatherModel:find():::, detalle: ' + error.message);
                }

            })
        })
    }

    this.save = function () {
        return new Promise((resolve, reject) => {
            db.client.hmset(this.weatherKey, this.weatherObject, (error, result) => {

                if (!error) {

                    if (result == 'OK') {
                        db.client.expire(this.weatherKey, process.env.EXPIRATION_TIME_KEY || 20);
                        resolve(this.weatherObject);

                    }

                } else {
                    reject('Error al guardar WeatherModel:save():::, detalle: ' + error.message);
                }

            })
        })

    }

}

module.exports = {
    WeatherModel
}