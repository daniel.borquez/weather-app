'use strict'
var Model = require('../models/weather');


async function findWeather(req, resp){

    await findWeatherDataDB(req,resp).catch(error => {
        console.error('Método findWeatherDataDB():::Ha ocurrido un error en el sistema, detalle: ' + error.message);
        resp.status(500).send({ message: 'Error en el servidor, intente más tarde' });
    });

}

async function saveWeather(req, resp){

    await saveWeatherDataDB(req,resp).catch(error => {
        console.error('Método saveWeatherDataDB():::Ha ocurrido un error en el sistema, detalle: ' + error.message);
        resp.status(500).send({ message: 'Error en el servidor, intente más tarde' });
    });

}

async function saveWeatherDataDB(req, resp) {

    let weatherModel = new Model.WeatherModel(
        req.body.latitude + 
        req.body.longitude, 
        req.body.temperature, 
        req.body.summary, 
        req.body.icon
    );

    await weatherModel.save().then(weather => {
        if (weather != null) {
            resp.status(200).send(weather);
        }else{
            resp.status(500).send({ message: 'Error en el servidor, intente más tarde' });
        }
    })



}



async function findWeatherDataDB(req, resp) {
    let weatherModel = new Model.WeatherModel(req.params.latitude + req.params.longitude);
    
    await weatherModel.find().then(weather => {

        if (weather != null) {
            //Enviando data cacheada desde redis
            resp.status(200).send(weather);
            //Se realiza el request al servicio de DarkSky para posteriormente guardar en redis
        }else{
            resp.status(404).send({message:'No se han encontrado datos de clima en BD', code: 404});

        }
    })
    

}


module.exports = {
    saveWeather,
    findWeather
}
