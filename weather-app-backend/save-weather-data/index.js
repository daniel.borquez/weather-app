'use strict'

const app = require('./app');
const db = require('./db');
require('dotenv').config();

//Conexión DB
db.client.on('connect', function () {
  console.log('Conectado a base de datos...');

});

db.client.on('error', function (error) {
  console.log('Error al conectar base de datos, detalle: ' + error);
  db.client.quit();

});




//Servidor a la escucha de peticiones HTTP
app.listen(process.env.PORT || 3001, function () {
  console.log('::: SERVICIO ::: SAVE-WEATHER-DATA corriendo en puerto ' + process.env.PORT || 3001);
});