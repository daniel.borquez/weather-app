const client = require('redis').createClient(process.env.DB_PORT || 6379, 'localhost');

module.exports = {
    client
}