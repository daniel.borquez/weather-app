'use strict'

var express = require('express');
var router = express.Router();
const SaveWeatherDataController = require('../controllers/save-weather-data-controller')

router.post('/save-weather', SaveWeatherDataController.saveWeather);
router.get('/find-weather/:latitude'+','+':longitude', SaveWeatherDataController.findWeather);


module.exports = router;
