'use strict';

function errorRespuesta(error, tipo) {


    let respuestaError = {
        codeError: '',
        messageError: ''
    };

    switch (tipo) {
        case 'general':
            if (error) {
                respuestaError.codeError = 500;
                respuestaError.messageError = 'Ha ocurrido un error en la aplicación, porfavor intente más tarde.';
            }
            break;

        case 'api':


            if (error.code == 400) {

                respuestaError.codeError = 400;
                respuestaError.messageError = 'No se pudieron encontrar las coordenadas, por favor intente en otro momento.';

            } else if (error.code == 403) {

                respuestaError.codeError = 403;
                respuestaError.messageError = 'La aplicación no cuenta con los permisos para consultar el servicio de meteorología, intente más tarde.';

            } else if (error.code == 404) {

                respuestaError.codeError = 404;
                respuestaError.messageError = 'El servicio meteorológico no se encuentra disponible en estos momentos, intente más tarde.';

            }
            break;

        case 'bd':

            if (error.code == 404) {
                console.log('llegamos')

                respuestaError.codeError = 404;
                respuestaError.messageError = 'No se encontraron registros del clima en la base de datos';

            } 
            break;


    }

    return respuestaError;

}

module.exports = {
    errorRespuesta: errorRespuesta
}