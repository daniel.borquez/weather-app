'use strict'
var Request = require("request");
let _handle = require("./utils/error-handling");
let respuestaError;

async function getWeather(req, resp) {

    await getWeatherData(req, resp).catch(err => {
        console.error('Ha ocurrido un error en el sistema, detalle: ' + err);
        resp.status(500).send({ message: 'Error en el servidor, intente más tarde' });
    });

};


async function getWeatherData(req, resp) {


    let coordenadas = {
        latitude: req.params.latitude,
        longitude: req.params.longitude
    }

    const weatherKey = coordenadas.latitude + coordenadas.longitude;

    //Se consulta primero si existe la key en bd
    await existsWeatherData(coordenadas).then(existsWeather => {

        if (existsWeather.code != null) {

            if (existsWeather.code == 404) {
                //Si no existen datos en la bd, se obtienen datos desde la api
                getWeatherApi(coordenadas).then(respuestaApi => {

                    if (respuestaApi.code != null) {
                        returnMessageError(respuestaApi, 'api', resp);

                    } else {
                        //Se guardan los datos en la bd para posterior consulta
                        saveWeatherDB(weatherKey, respuestaApi).then(respuestaSaveDb => {

                            if (respuestaSaveDb.code != null) {
                                returnMessageError(respuestaSaveDb, 'bd', resp);

                            } else {
                                //Se envían datos guardados al cliente
                                resp.status(200).send(respuestaSaveDb);


                            }
                        });
                    }
                });

            } else {
                returnMessageError(existsWeather, 'bd', resp);

            }


        } else {

            //Si es que existe la key en la bd se retorna
            if (existsWeather != null) {
                resp.status(200).send(existsWeather);

            }
        }
    });

}



async function getWeatherApi(coordenadas) {

    return new Promise((resolve, reject) => {
        Request.get(process.env.API_WEATHER_EXTERN_URL
            + coordenadas.latitude + ","
            + coordenadas.longitude +
            process.env.COMODINES_API_URL,
            (error, response, body) => {

                if (!error) {

                    if (JSON.parse(body).code != null) {

                        resolve(JSON.parse(body));

                    } else {

                        let respuestaApiMapeada = {
                            latitude: JSON.parse(body).latitude.toString(),
                            longitude: JSON.parse(body).longitude.toString(),
                            temperature: JSON.parse(body).currently.temperature,
                            summary: JSON.parse(body).currently.summary,
                            icon: JSON.parse(body).currently.icon,

                        }
                        resolve(respuestaApiMapeada);
                    }

                } else {
                    reject('Método (getWeatherApi):::' + error.message);
                }

            })

    });


}

async function existsWeatherData(coordenadas) {


    return new Promise((resolve, reject) => {

        Request.get(process.env.DB_GET_WEATHER_URL
            + coordenadas.latitude + ","
            + coordenadas.longitude,
            (error, response, body) => {

                if (!error) {
                    resolve(JSON.parse(body));

                } else {
                    reject('Método (existsWeatherData):::' + error.message);
                }
            });
    })

}




async function saveWeatherDB(weatherKey, weatherApi) {

    return new Promise((resolve, reject) => {

        Request.post({
            url: process.env.DB_SAVE_WEATHER_URL,
            body: weatherApi,
            json: true

        }, (error, response, body) => {

            if (!error) {
                resolve(body);

            } else {
                reject('Método (saveWeatherDB):::' + error.message);
            }
        });

    })

}

function returnMessageError(error, tipo, resp) {

    switch (tipo) {
        case 'api':
            respuestaError = _handle.errorRespuesta(error, 'api');
            console.log('getWeather:::Error en servicio de clima, causa:', respuestaError.messageError);
            break;

        case 'bd':
            respuestaError = _handle.errorRespuesta(error, 'bd');
            console.log('getWeather:::Error en servicio de BD, causa:', respuestaError.messageError);
            break;

    }
    resp.status(respuestaError.codeError).send({ message: respuestaError.messageError });



}


module.exports = {
    getWeather
}
