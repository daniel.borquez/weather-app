'use strict'

var express = require('express');
var router = express.Router();
const GetWeatherDataController = require('../controllers/get-weather-data-controller')

router.get('/get-weather/:latitude'+','+':longitude', GetWeatherDataController.getWeather);


module.exports = router;
