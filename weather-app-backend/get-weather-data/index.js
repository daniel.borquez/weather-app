'use strict'

const app = require('./app');
require('dotenv').config();

//Servidor a la escucha de peticiones HTTP
app.listen(process.env.PORT||3000, function () {

    console.log('::: SERVICIO ::: GET-WEATHER-DATA corriendo en puerto '+process.env.PORT);
    
  });