'use strict'

const express = require('express');
let app = express();
let bodyParser = require('body-parser');

const GetWeatherDataRoutes = require('./routes/get-weather-data-routes');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
  
app.use('/get-weather-data', GetWeatherDataRoutes);


module.exports = app;
