import React from 'react';
import './App.css';
require.context('../public', true, /^\.\//);

import WeatherMap from './components/WeatherMap'


function App() {
  return (

    <div className="App">

      <WeatherMap />

    </div>
  );
}

export default App;
