import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import GoogleMapRender from './GoogleMapRender';
import cloudy from '../../public/cloudy.png';
import sunny from '../../public/sunny.png';
import moon from '../../public/moon.png';
import rain from '../../public/rain.png';

let imagen;

class WeatherMap extends React.Component {

  constructor(props, context) {
    super(props, context);


    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.handleShowNotificacion = this.handleShowNotificacion.bind(this);
    this.handleCloseNotificacion = this.handleCloseNotificacion.bind(this);

    this.state = {
      imagen: '',
      show: false,
      showNotificacion: false,
      messageModal: '',
      season: '',
      selectedCountry: {
        country: '',
        capital: '',
        latitude: '',
        longitude: ''
      },
      weatherInfo: {
        temperature: 0,
        summary: '',
        station: '',
        icon: ''
      }
    };

  }

  determineSeason(desdeParameter, hastaParameter, fechaActualParameter) {

    var d1 = desdeParameter.split("/");
    var d2 = hastaParameter.split("/");
    var c = fechaActualParameter.split("/");

    var desde = new Date(d1[2], parseInt(d1[1]) - 1, d1[0]);
    var hasta = new Date(d2[2], parseInt(d2[1]) - 1, d2[0]);
    var fechaActual = new Date(c[2], parseInt(c[1]) - 1, c[0]);

    if (fechaActual >= desde && fechaActual <= hasta) {
      return true;
    } else {
      return false
    }

  }

  getActualDate() {
    var todayTime = new Date();
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();

    let fechaActual = day + '/' + month + '/' + year;

    return fechaActual;
  }

  selectSeason(latitude) {

    let polo = '';
    var todayTime = new Date();

    let estacionLocacion = '';

    if(parseFloat(latitude) >= 0) {
      //hemisferio norte

      if (this.determineSeason('21/03/' + todayTime.getFullYear(), '20/06/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Primavera';
      } else if (this.determineSeason('21/06/' + todayTime.getFullYear(), '20/09/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Verano';

      } else if (this.determineSeason('21/09/' + todayTime.getFullYear(), '20/12/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Otoño';

      } else if (this.determineSeason('21/12/' + todayTime.getFullYear(), '20/03/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Invierno';

      }


    }else{

      //hemisferio sur
      if (this.determineSeason('21/09/' + todayTime.getFullYear(), '20/12/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Primavera';
      } else if (this.determineSeason('21/12/' + todayTime.getFullYear(), '20/03/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Verano';

      } else if (this.determineSeason('21/03/' + todayTime.getFullYear(), '20/06/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Otoño';

      } else if (this.determineSeason('21/06/' + todayTime.getFullYear(), '20/09/' + todayTime.getFullYear(), this.getActualDate())) {
        estacionLocacion = 'Invierno';

      }

    }


    


    this.setState({
      season: estacionLocacion
    })


  }

  selectIcon(icon) {
    let imagenClima;
    switch (icon) {

      case 'cloudy-day':
      case 'cloudy-night':
      case 'partly-cloudy-day':
      case 'cloudy':
        imagenClima = cloudy;
        break;

      case 'clear-day':
      case 'partly-clear-day':
      case 'clear':
        imagenClima = sunny;

        break;

      case 'clear-night':
        imagenClima = moon;

        break;

      case 'rain-day':
      case 'rain-night':
      case 'partly-rain-day':
      case 'rain':
        imagenClima = rain;

        break;

      case 'cloudy-day':
      case 'cloudy-night':
      case 'partly-cloudy-day':
      case 'cloudy':
        imagenClima = cloudy;

        break;

      default:
        imagenClima = cloudy;

        break;

    }

    imagen = imagenClima;

  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleCloseNotificacion() {
    this.setState({ showNotificacion: false });
  }

  handleShowNotificacion() {
    this.setState({ showNotificacion: true });
  }

  selectCountry(map) {

    if (map != null && !map['row'] != null) {

      this.selectSeason(map['row'].latitude.value);
      this.setState({

        selectedCountry: {
          country: map['row'].country.value,
          capital: map['row'].capital.value,
          latitude: map['row'].latitude.value,
          longitude: map['row'].longitude.value
        },

      })

      this.getWeather();


    }

  }

  getWeather() {
    fetch('http://weather-app-get.acidlabs.com:3000/get-weather-data/get-weather/' + this.state.selectedCountry.latitude + ',' + this.state.selectedCountry.longitude
    ).then(resolve => {

      return resolve.json();
    }).then(data => {


      if (data.message == null) {

        this.setState({

          weatherInfo: {
            temperature: parseInt(data.temperature),
            summary: data.summary,
            icon: data.icon
          }

        });

        this.selectIcon(this.state.weatherInfo.icon);

        this.handleShow();
      } else {

        this.setState({

          messageModal: data.message

        });

        this.handleShowNotificacion();

      }
    }).catch(rejected => {

      console.log('Petición rechazada, detalle: ', rejected);

      this.setState({
        messageModal: 'En estos momentos, el servidor no se encuentra disponible, intente más tarde.',
        showNotificacion: true
      });

    });
  }


  render() {
    return (
      <>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title style={{fontSize: 20}}>Pronóstico de {this.state.season}</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ 'textAlign': 'center', 'textSizeAdjust': '20' }} className="mb-15 text-muted color-primary">
            <img src={imagen} height="100" width="100"></img> {this.state.weatherInfo.temperature} Cº {this.state.weatherInfo.summary} </Modal.Body>
          <Modal.Footer>
            {this.state.selectedCountry.capital}, {this.state.selectedCountry.country}.
          </Modal.Footer>
        </Modal>


        <Modal show={this.state.showNotificacion} onHide={this.handleCloseNotificacion}>
          <Modal.Header>
            <Modal.Title>Información</Modal.Title>
          </Modal.Header>
          <Modal.Body className="mb-2 text-muted">{this.state.messageModal}</Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleCloseNotificacion}>
              Cerrar
            </Button>

          </Modal.Footer>
        </Modal>

        <GoogleMapRender selectCountry={this.selectCountry.bind(this)} />
      </>
    )
  }

}


export default WeatherMap;