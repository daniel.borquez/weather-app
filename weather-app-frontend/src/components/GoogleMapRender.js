import React from 'react';
const { compose, withProps } = require("recompose");
const { withScriptjs, withGoogleMap, GoogleMap, FusionTablesLayer } = require("react-google-maps");

const GoogleMapRender = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDeVAAkJ_oKtGlN-oMqOaE10_XBryAQnBU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `800px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>

  <GoogleMap
    defaultZoom={4}
    defaultCenter={{ lat: 40.854885, lng: -88.081807 }}
    options={{
      disableDoubleClickZoom: true,
      zoomControl: false,
      fullscreenControl: false,
      scrollwheel: false,
      streetViewControl: false


    }}
  >


    <FusionTablesLayer
      url="http://www.geocodezip.com/geoxml3_test/world_countries_kml.xml"
      options={{
        query: {
          select: `country, capital, latitude, longitude`,
          from: `1yV2xdwoCJYeKPy_5DyWYwaTd90wNh5tn3nX9_LE`,
          where: `capital not in ('Sucre (judicial)')`,
          
        },
        suppressInfoWindows: true
      }}
      onClick={props.selectCountry}
    />
  </GoogleMap>
);

export default GoogleMapRender;