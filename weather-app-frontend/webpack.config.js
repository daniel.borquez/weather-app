var path = require('path');
var HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {

	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'main.js'
	},

	module: {
		rules: [

			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader', options: { presets: ['@babel/preset-env', '@babel/preset-react'] } },
			},

			{
				test: /\.(css)$/,
				use: [
					{ loader: 'style-loader' },
					// css-loader
					{
						loader: 'css-loader',
						options: { modules: true }
					},


					// sass-loader
					/*{ loader: 'sass-loader' }*/
				]
			},

			{
				test: /\.(jpg|jpeg|gif|png|ico|json)$/,
				exclude: /node_modules/,
				loader: 'file-loader',
				options: { name: '[name].[ext]', context: '/public/' }
			
			
			}

		]
	},

	mode: 'development',
	plugins: [
		new HtmlWebPackPlugin({

			template: 'public/index.html'

		})

	]
}



